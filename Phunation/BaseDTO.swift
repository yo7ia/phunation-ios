//
//  BaseDTO.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation

class BaseDTO {
    var id: String = "";
    var type: String = "";
    
    init(dic: [String: AnyObject]) {
    self.id = dic["_id"] as! String
    self.type = dic["type"] as! String
            
        }
}
