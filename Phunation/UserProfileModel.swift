//
//  UserProfileModel.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation


protocol UserProfileDelegate {
    func userProfileModel(model: UserProfileModel, user: UserProfile?, errorMessage: String?)
}

class UserProfileModel: BaseModel{
    
    override init() {
        super.init()
    }
    
    private let GET_DERIVERS_URL = "http://52.42.186.30:4985/phonation-syncgw/user_profile::user::5565150c858db53b39f7004385d40c01"
    var deleget: UserProfileDelegate?
    var session: NSURLSession!
    
    
    func getUserProfile() {
        
        //Create request from URL
        let  driversDataUrl =  NSURL(string:GET_DERIVERS_URL);
        
        let request = NSURLRequest(URL:driversDataUrl!);
        
        //Send Asyncronus request
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        session = NSURLSession(configuration: config)
        
        
        
       
        let task = session.dataTaskWithRequest(request) { (resData :NSData?, response: NSURLResponse?, err: NSError?) -> Void in
            
            //Handle connection error
            if (err != nil) {
                print("Connection error : \(err!.localizedDescription)");
                self.deleget?.userProfileModel(self, user: nil, errorMessage: err!.localizedDescription)
                return;
            } else if let data = resData {
                
                do {
                    //PArse JSON
                    let jsonDictionary = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? [String: AnyObject]
                    let userData = UserProfile(dic: jsonDictionary!)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                         self.deleget?.userProfileModel(self, user: userData, errorMessage: nil)
                    })
                   
                    
                    //Parsing error (invalid JSON)
                } catch let JSONError as NSError {
                    print("Error: \(JSONError.localizedDescription)")
                    self.deleget?.userProfileModel(self, user: nil, errorMessage: JSONError.localizedDescription)
                    return
                }
            } else {
                self.deleget?.userProfileModel(self, user: nil, errorMessage: "No Drivers recived")
            }
        }
        task.resume()
    }
}