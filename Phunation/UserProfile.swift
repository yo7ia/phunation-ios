//
//  UserProfile.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation

class UserProfile : BaseDTO {
    
    var name = ""
    var username = ""
    var email: String = "";
    var gender = Gender.PreferNotToSay;
    var country = ""
    var favorited_items =  [String]()
    var followed_providers = [String]()
    var supported_countries = [String]()
    var main_image = ""
    var user_id = ""
    var phone = ""
    var birthdate: NSNumber? = 0;
//    var genderType: Gender? {
//        guard let gender = gender else { return nil }
//        return Gender(rawValue: gender)
    
    override init(dic: [String: AnyObject]) {
        super.init(dic: dic)
        self.name = dic["name"] as! String
        self.username = dic["username"] as! String
        self.email = dic["email"] as! String
        self.country = dic["country"] as! String
        self.favorited_items = dic["favorited_items"] as! [String]
        self.followed_providers = dic["followed_providers"] as! [String]
        self.supported_countries = dic["supported_countries"] as! [String]
        self.main_image = dic["main_image"] as! String
        self.phone = dic["phone"] as! String
        self.user_id = dic["user_id"] as! String
        self.gender = Gender(rawValue: dic["gender"] as! String)!
        self.birthdate = dic["birthdate"] as! NSNumber? 
    }
}
