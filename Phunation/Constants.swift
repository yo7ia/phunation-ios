//
//  Constants.swift
//  DarAlMaarefa
//
//  Created by Ahmed Ibrahim on 6/15/16.
//  Copyright © 2016 AMIT. All rights reserved.
//

import Foundation

struct Constants {
    
    struct ImageBaseURL {
        static let ImageURL = "http://52.42.186.30/books-backend/uploads/"
    }
    
    struct Couch {
        static let DatabaseName = "phonation-syncgw"
        static let SyncgatewayURL = NSURL(string: "http://52.42.186.30:4985/\(DatabaseName)")!
        
        struct DocumentType {
            static let UserProfile = "user_profile"
            static let UserCart = "user_cart"
            static let UserOrders = "order"
            static let UserNotification = "user_notification"
            
            static let Category = "category"
            
            static let ProviderCalender = "provider_calender"
            static let Provider = "provider_profile"
           
            static let ServiceReview = "review"
            static let Service = "service"
        }
        
        
        struct View {
            
            static let AllServices = "AllServices"
            static let AllCategories = "AllCategories"
            static let ServicesPerCategory = "ServicesPerCategory"
        }
    }
    
    struct APIProvider {
        static let APIBaseURL = "http://52.42.186.30/phunation/dev"
        static let Login = NSURL(string: APIBaseURL + "/api/login")!
        static let Signup = NSURL(string: APIBaseURL + "/api/register")!
        static let UpdateProfile = NSURL(string: APIBaseURL + "/api/profile")!
        static let ResetPassword = NSURL(string: APIBaseURL + "/api/reset-password")!
        static let ResendActivation = NSURL(string: APIBaseURL + "/api/resend-activation")!
        static let RegisterNotification = NSURL(string: APIBaseURL + "/api/register-notification")!

    }
    
}