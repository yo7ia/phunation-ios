//
//  FirstViewController.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController , UserProfileDelegate {
    
    var model: UserProfileModel!
    //MARK: life cycle
    override func viewDidLoad() {
    super.viewDidLoad()
    getUserProfileData()
    }
    
    //MARK: Private
    private func getUserProfileData() {
    model = UserProfileModel()
    model.deleget = self
    model.getUserProfile()
    }
    
    //MARK: DriversModelDelegate
    func userProfileModel(model: UserProfileModel, user: UserProfile?, errorMessage: String?) {
      print((user?.gender.displayValue)! + (user?.id)!)
    }


}

