//
//  Gender.swift
//  Phunation
//
//  Created by AMIT Software on 10/18/16.
//  Copyright © 2016 AMIT Software. All rights reserved.
//

import Foundation

enum Gender: String {
    case Male = "m"
    case Female = "f"
    case PreferNotToSay = "o"
}

extension Gender {
    
    var displayValue: String {
        
        switch self {
        case .Male:
            return NSLocalizedString("Male", comment: "")
            
        case .Female:
            return NSLocalizedString("Female", comment: "")
            
        case .PreferNotToSay:
            return NSLocalizedString("Prefere not to say", comment: "")
        }
        
    }
    
}