//
//  NFSerialization.h
//  NFNetwork
//
//  Created by chris on 16/6/3.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>


@class AFHTTPRequestSerializer;
@class AFHTTPResponseSerializer;

typedef NS_ENUM(NSInteger, NFRequestSerializationType) {
    NFRequestSerializationTypeNone,
    NFRequestSerializationTypeHttp,
    NFRequestSerializationTypeJson,
    NFRequestSerializationTypePropertyList
};

typedef NS_ENUM(NSInteger, NFResponseSerializationType) {
    NFResponseSerializationTypeNone,
    NFResponseSerializationTypeHttp,
    NFResponseSerializationTypeJson,
    NFResponseSerializationTypePropertyList,
    NFResponseSerializationTypeImage
};

@interface NFSerialization : NSObject

+ (AFHTTPRequestSerializer*) requestSerializerByType:(NFRequestSerializationType)type;

+ (AFHTTPResponseSerializer*) responseSerializerByType:(NFResponseSerializationType)type;

@end
