//
//  NFHttpRequest.h
//  NetworkFramework
//
//  Created by chris on 16/5/30.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NFHttpRequestDatasource.h"


@interface NFUploadData : NSObject

@property(nonatomic, strong)    NSData          *formData;
@property(nonatomic, copy)      NSString        *name;
@property(nonatomic, copy)      NSString        *fileName;
@property(nonatomic, copy)      NSString        *mimeType;

@end


typedef void(^NFNetworkSuccess)(id responseObject);
typedef void(^NFNetworkFailure)(NSError *error);

@interface NFHttpRequest : NSObject<NFHttpRequestDatasource>


/**
 *  class methods
 */
+ (void) setSchema:(NSString*)schema;

+ (NSString*) schema;

+ (void) setHost:(NSString*)host;   //host with/without port

+ (NSString*) host;

+ (void) setPathPrefix:(NSString*)pathPrefix;

+ (NSString*) pathPrefix;

+ (void) setHeaders:(NSDictionary*)headers;

+ (NSDictionary*) headers;

+ (void) setRequestSerializationType:(NFRequestSerializationType)type;

+ (NFRequestSerializationType) requestSerializationType;

+ (void) setResponseSerializationType:(NFResponseSerializationType)type;

+ (NFResponseSerializationType) responseSerializationType;

/**
 *  interface method
 */
- (NSURLSessionDataTask*) sendWithSuccess:(NFNetworkSuccess)success failure:(NFNetworkFailure)failure;

- (NSMutableURLRequest *) toURLRequest;


@end
