//
//  NFFormRequest.m
//  NFNetwork
//
//  Created by chris on 16/6/3.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import "NFFormRequest.h"
#import "AFURLRequestSerialization.h"
#import "NFSerialization.h"

@implementation NFFormRequest

- (NSMutableURLRequest*) toURLRequest{
    
    
    NSError *error = nil;
    
    AFHTTPRequestSerializer *requestSerializer = [NFSerialization requestSerializerByType:[self requestSerializationType]];
    NSMutableURLRequest *request = [requestSerializer multipartFormRequestWithMethod:[self method] URLString:[self url] parameters:[self parameters] constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        for(NFUploadData *data in [self formDataList]){
            [formData appendPartWithFileData:data.formData name:data.name fileName:data.fileName mimeType:data.mimeType];
        }
        
    } error:&error];
 
    return request;
}

@end
