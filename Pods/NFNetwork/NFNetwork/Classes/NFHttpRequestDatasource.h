//
//  NFHttpRequestDatasource.h
//  NFNetwork
//
//  Created by chris on 16/6/2.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NFSerialization.h"

@class NFHttpRequest;

@protocol NFHttpRequestDatasource <NSObject>

@optional
/**
 *  instance methods optional inherit
 */
- (NSString*) method;       //http method

- (NSString*) url;

- (NSDictionary*) headers;

- (NSDictionary*) parameters;

- (NSArray*) formDataList;

- (NFRequestSerializationType) requestSerializationType;

- (NFResponseSerializationType) responseSerializationType;


/**
 *  instance method must inherit
 */
- (NSString*) path;

@end
