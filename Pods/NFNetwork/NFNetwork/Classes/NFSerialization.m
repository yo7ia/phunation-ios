//
//  NFSerialization.m
//  NFNetwork
//
//  Created by chris on 16/6/3.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import "NFSerialization.h"
#import <AFNetworking.h>

@implementation NFSerialization


+ (AFHTTPRequestSerializer*) requestSerializerByType:(NFRequestSerializationType)type{
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    if(type==NFRequestSerializationTypeJson){
        requestSerializer = [AFJSONRequestSerializer serializer];
    }
    else if(type==NFRequestSerializationTypePropertyList){
        requestSerializer = [AFPropertyListRequestSerializer serializer];
    }
    
    return requestSerializer;
}

+ (AFHTTPResponseSerializer*) responseSerializerByType:(NFResponseSerializationType)type{
    
    AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
    
    if(type==NFResponseSerializationTypeJson){
        responseSerializer = [AFJSONResponseSerializer serializer];
    }
    else if(type==NFResponseSerializationTypePropertyList){
        responseSerializer = [AFPropertyListResponseSerializer serializer];
    }
    else if(type==NFResponseSerializationTypeImage){
        responseSerializer = [AFImageResponseSerializer serializer];
    }
    
    return responseSerializer;
}

@end
