//
//  NFHttpRequest.m
//  NetworkFramework
//
//  Created by chris on 16/5/30.
//  Copyright © 2016年 group4app. All rights reserved.
//

#import "NFHttpRequest.h"
#import <objc/runtime.h>
#import "AFURLRequestSerialization.h"
#import "AFHTTPSessionManager.h"
#import "NFSerialization.h"

#define kSchema         @"schema"
#define kHost           @"host"
#define kPathPrefix     @"path_prefix"
#define kHeader         @"header"
#define kRequestSerialization   @"request"
#define kResponseSerialization  @"response"

static NSMutableDictionary  *c_envInfo = nil;


@implementation NFUploadData


@end


@implementation NFHttpRequest

#pragma mark - class methods
+ (NSMutableDictionary*) envInfo{
    if(!c_envInfo){
        c_envInfo = [NSMutableDictionary dictionary];
    }
    return c_envInfo;
}

+ (void) setValue:(NSObject*)value forKey:(NSString*)key{
    
    NSMutableDictionary *envInfo = [self envInfo];
    envInfo[key] = value;
}

+ (NSObject*) valueForKey:(NSString*)key{
    
    NSMutableDictionary *envInfo = [self envInfo];
    return envInfo[key];
}


+ (void) setSchema:(NSString *)schema{
    
    [self setValue:schema forKey:kSchema];
}

+ (NSString*) schema{
    
    return (NSString*)[self valueForKey:kSchema];
}

+ (void) setHost:(NSString *)host{
    
    [self setValue:host forKey:kHost];
}

+ (NSString*) host{
    
    return (NSString*)[self valueForKey:kHost];
}

+ (void) setPathPrefix:(NSString *)pathPrefix{
    
    [self setValue:pathPrefix forKey:kPathPrefix];
}

+ (NSString*) pathPrefix{
    
    return (NSString*)[self valueForKey:kPathPrefix];
}

+ (void) setHeaders:(NSDictionary *)headers{
    
    [self setValue:headers forKey:kHeader];
}

+ (NSDictionary*) headers{
    
    return (NSDictionary*)[self valueForKey:kHeader];
}

+ (void) setRequestSerializationType:(NFRequestSerializationType)type{
    
    [self setValue:@(type) forKey:kRequestSerialization];
}

+ (NFRequestSerializationType) requestSerializationType{
    
    NSNumber *type = (NSNumber*)[self valueForKey:kRequestSerialization];
    
    return [type integerValue];
}

+ (void) setResponseSerializationType:(NFResponseSerializationType)type{
    
    [self setValue:@(type) forKey:kResponseSerialization];
}

+ (NFResponseSerializationType) responseSerializationType{
    
    NSNumber *type = (NSNumber*)[self valueForKey:kResponseSerialization];
    return [type integerValue];
}

#pragma mark - instance methods optional inherit
- (NSString *) method{
    
    return @"POST";
    
}

- (NSString *) url{
    
    NSString *schema = [NFHttpRequest schema];
    NSString *host = [NFHttpRequest host];
    NSString *prefix = [NFHttpRequest pathPrefix];
    prefix = prefix?prefix:@"";
    NSString *path = [self path];
    return [NSString stringWithFormat:@"%@://%@/%@%@",schema,host,prefix,path];
}


- (NSDictionary*) parameters{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dict setObject:[self valueForKey:key] forKey:key];
    }
    
    free(properties);
    
    return [NSDictionary dictionaryWithDictionary:dict];
}

- (NSDictionary*) headers{
    
    NSMutableDictionary *envInfo = [NFHttpRequest envInfo];
    return envInfo[kHeader];
}

- (NSArray*) formDataList{
    
    return nil;
}

- (NFRequestSerializationType) requestSerializationType{
    
    return  NFRequestSerializationTypeHttp;
}

- (NFResponseSerializationType) responseSerializationType{
    
    return NFResponseSerializationTypeHttp;
}

#pragma mark - instance mehtod must inherit
- (NSString *) path{
    
    return @"";
}


#pragma mark - interface
- (NSURLSessionDataTask*) sendWithSuccess:(NFNetworkSuccess)success failure:(NFNetworkFailure)failure{
    
    NSMutableURLRequest *request = [self toURLRequest];
    [self configHeadersForRequest:request];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [NFSerialization responseSerializerByType:[self responseSerializationType]];
    
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if(error){
            failure(error);
        }
        else{
            success(responseObject);
        }
    }];
    
    [task resume];
    
    return task;
}


- (NSMutableURLRequest*) toURLRequest{
    
    NSError *error = nil;
    
    AFHTTPRequestSerializer *requestSerializer = [NFSerialization requestSerializerByType:[self requestSerializationType]];
    NSMutableURLRequest *request = [requestSerializer requestWithMethod:[self method] URLString:[self url] parameters:[self parameters] error:&error];
    
    if(error){
        NSLog(@"create request error = %@",error);
    }
    
    return request;
}



- (void) configHeadersForRequest:(NSMutableURLRequest*)request{
    
    NSDictionary *parameters = [self parameters];
    for(NSString *key in [parameters allKeys]){
        [request addValue:parameters[key] forHTTPHeaderField:key];
    }
}

@end
