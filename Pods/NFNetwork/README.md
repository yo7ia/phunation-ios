# NFNetwork

[![CI Status](http://img.shields.io/travis/ruxue.huang/NFNetwork.svg?style=flat)](https://travis-ci.org/ruxue.huang/NFNetwork)
[![Version](https://img.shields.io/cocoapods/v/NFNetwork.svg?style=flat)](http://cocoapods.org/pods/NFNetwork)
[![License](https://img.shields.io/cocoapods/l/NFNetwork.svg?style=flat)](http://cocoapods.org/pods/NFNetwork)
[![Platform](https://img.shields.io/cocoapods/p/NFNetwork.svg?style=flat)](http://cocoapods.org/pods/NFNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NFNetwork is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NFNetwork"
```

## Author

hrx00747, hrx00747@gmail.com

## License

NFNetwork is available under the MIT license. See the LICENSE file for more info.
